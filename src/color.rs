use std::cmp::Ordering;
use std::convert::TryFrom;
use std::fmt::{self, Display, Formatter};
use std::str::FromStr;

#[derive(Clone, Copy, Hash, PartialEq, Eq, Debug)]
pub enum Color {
    Black,
    Blue,
    Green,
    LightBlue,
    LightGreen,
    Orange,
    Pink,
    Purple,
    Red,
    Turquoise,
    White,
    Yellow,
}

impl Color {
    pub const EMPTY: Self = Self::White;

    pub const NUM_COLORS: usize = 12;
    pub const ALL: [Self; Self::NUM_COLORS] = [
        Self::Black,
        Self::Blue,
        Self::Green,
        Self::LightBlue,
        Self::LightGreen,
        Self::Orange,
        Self::Pink,
        Self::Purple,
        Self::Red,
        Self::Turquoise,
        Self::White,
        Self::Yellow,
    ];

    const NUM_NEIGHBORS: usize = 5;
    #[rustfmt::skip]
    const NEIGHBORS: [[Self; Self::NUM_NEIGHBORS]; Self::NUM_COLORS] = [
        [Self::LightGreen, Self::Pink, Self::Turquoise, Self::White, Self::Yellow],
        [Self::LightBlue, Self::Orange, Self::Purple, Self::Turquoise, Self::Yellow],
        [Self::LightBlue, Self::LightGreen, Self::Orange, Self::Red, Self::White],
        [Self::Blue, Self::Green, Self::Orange, Self::Turquoise, Self::White],
        [Self::Black, Self::Green, Self::Pink, Self::Red, Self::White],
        [Self::Blue, Self::Green, Self::LightBlue, Self::Purple, Self::Red],
        [Self::Black, Self::LightGreen, Self::Purple, Self::Red, Self::Yellow],
        [Self::Blue, Self::Orange, Self::Pink, Self::Red, Self::Yellow],
        [Self::Green, Self::LightGreen, Self::Orange, Self::Pink, Self::Purple],
        [Self::Black, Self::Blue, Self::LightBlue, Self::White, Self::Yellow],
        [Self::Black, Self::Green, Self::LightBlue, Self::LightGreen, Self::Turquoise],
        [Self::Black, Self::Blue, Self::Pink, Self::Purple, Self::Turquoise],
    ];

    const NAMES: [&'static str; Self::NUM_COLORS] = [
        "black",
        "blue",
        "green",
        "lightblue",
        "lightgreen",
        "orange",
        "pink",
        "purple",
        "red",
        "turquoise",
        "white",
        "yellow",
    ];

    const OPPOSITE: [Self; Self::NUM_COLORS] = [
        Color::Orange,
        Color::LightGreen,
        Color::Yellow,
        Color::Pink,
        Color::Blue,
        Color::Black,
        Color::LightBlue,
        Color::White,
        Color::Turquoise,
        Color::Red,
        Color::Purple,
        Color::Green,
    ];

    pub fn neighbors(&self) -> &'static [Self] {
        &Self::NEIGHBORS[usize::from(self)]
    }

    pub fn opposite(&self) -> Self {
        Self::OPPOSITE[usize::from(self)]
    }

    pub fn has_ball(&self) -> bool {
        *self != Self::EMPTY
    }
}

impl Display for Color {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        f.pad(Self::NAMES[usize::from(self)])
    }
}

impl FromStr for Color {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Self::NAMES
            .iter()
            .position(|&name| name == s)
            .map(|i| Self::try_from(i).unwrap())
            .ok_or_else(|| format!("Unknown color '{}'", s))
    }
}

impl TryFrom<usize> for Color {
    type Error = String;

    fn try_from(index: usize) -> Result<Self, Self::Error> {
        match index {
            0 => Ok(Self::Black),
            1 => Ok(Self::Blue),
            2 => Ok(Self::Green),
            3 => Ok(Self::LightBlue),
            4 => Ok(Self::LightGreen),
            5 => Ok(Self::Orange),
            6 => Ok(Self::Pink),
            7 => Ok(Self::Purple),
            8 => Ok(Self::Red),
            9 => Ok(Self::Turquoise),
            10 => Ok(Self::White),
            11 => Ok(Self::Yellow),
            x => Err(format!("{} is out of range", x)),
        }
    }
}

impl From<Color> for usize {
    fn from(color: Color) -> usize {
        usize::from(&color)
    }
}

impl From<&Color> for usize {
    fn from(color: &Color) -> usize {
        match color {
            Color::Black => 0,
            Color::Blue => 1,
            Color::Green => 2,
            Color::LightBlue => 3,
            Color::LightGreen => 4,
            Color::Orange => 5,
            Color::Pink => 6,
            Color::Purple => 7,
            Color::Red => 8,
            Color::Turquoise => 9,
            Color::White => 10,
            Color::Yellow => 11,
        }
    }
}

impl Ord for Color {
    fn cmp(&self, other: &Self) -> Ordering {
        usize::from(self).cmp(&other.into())
    }
}

impl PartialOrd for Color {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}
