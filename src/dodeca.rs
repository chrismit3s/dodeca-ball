use crate::color::Color;
use crate::solver::Solver;
use std::fmt::{self, Display, Formatter};
use std::hash::{Hash, Hasher};

type Layout = [Color; Color::NUM_COLORS];

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub struct Dodeca {
    layout: Layout,
}

impl Dodeca {
    pub const SOLVED: Self = Self { layout: Color::ALL };
    pub const SOLVED_INDEX: usize = 0;

    pub const TWELVE_FAC: usize = 1 * 2 * 3 * 4 * 5 * 6 * 7 * 8 * 9 * 10 * 11 * 12; // = 12!

    pub fn new(layout: Layout) -> Self {
        let mut mask: u16 = 0;
        for color in layout.iter() {
            mask |= 1 << usize::from(color);
        }
        if mask != !(!0 << Color::NUM_COLORS) {
            panic!("Bad layout {:?}", layout);
        }

        Self { layout }
    }

    pub fn after_swap(&self, color: Color) -> Self {
        let mut copy = self.clone();
        copy.swap(color);
        copy
    }

    pub fn empty(&self) -> Color {
        Color::try_from(self.layout.iter().position(|&c| c == Color::EMPTY).unwrap()).unwrap()
    }

    pub fn possible_swaps(&self) -> &'static [Color] {
        self.empty().neighbors()
    }

    pub fn swap(&mut self, color: Color) {
        let empty = self.empty();
        self.layout.swap(color.into(), empty.into());
    }

    pub fn solve_with(&self, solver: &mut Solver) -> Option<Vec<Color>> {
        let index = self.into();
        solver.search_for(index);
        solver.path(index)
    }

    pub fn solve(&self) -> Option<Vec<Color>> {
        self.solve_with(&mut Solver::new())
    }
}

impl From<usize> for Dodeca {
    fn from(x: usize) -> Self {
        let x = x as usize;
        let inversions = (0..12).rev().scan(Self::TWELVE_FAC, |state, i| {
            *state /= i + 1;
            Some((x / *state) % (i + 1))
        });

        let mut colors: [Option<Color>; Color::NUM_COLORS] = Color::ALL
            .iter()
            .copied()
            .map(Some)
            .collect::<Vec<_>>()
            .try_into()
            .unwrap();

        let mut layout = Color::ALL.clone();
        for (i, x) in inversions.enumerate() {
            layout[i] = colors
                .iter_mut()
                .filter(|color| color.is_some())
                .nth(x)
                .unwrap()
                .take()
                .unwrap();
        }

        Self::new(layout)
    }
}

impl From<Dodeca> for usize {
    fn from(dodeca: Dodeca) -> usize {
        usize::from(&dodeca)
    }
}

impl From<&Dodeca> for usize {
    fn from(dodeca: &Dodeca) -> usize {
        let inversions = dodeca.layout.iter().enumerate().map(|(i, icolor)| {
            dodeca.layout[(i + 1)..]
                .iter()
                .filter(|jcolor| icolor > jcolor)
                .count()
        });

        let mut code = 0;
        for (i, x) in inversions.enumerate() {
            code = code * (Color::NUM_COLORS - i) + x;
        }
        code as usize
    }
}

impl Hash for Dodeca {
    fn hash<H: Hasher>(&self, state: &mut H) {
        let x: usize = self.into();
        x.hash(state);
    }
}

impl Display for Dodeca {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        for color in Color::ALL {
            writeln!(f, "{}: {}", color, self.layout[usize::from(color)])?;
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_solved_code_zero() {
        let x: usize = Dodeca::SOLVED.into();
        assert_eq!(x, Dodeca::SOLVED_INDEX);
        let x = Dodeca::from(Dodeca::SOLVED_INDEX);
        assert_eq!(x, Dodeca::SOLVED);
    }

    #[test]
    fn test_from_into_fixpoint() {
        for i in (0..Dodeca::TWELVE_FAC).step_by(12345) {
            let x = Dodeca::from(i);
            println!("i={:?} layout={:?}", i, x.layout);
            let j: usize = x.into();
            assert_eq!(i, j);
        }
    }
}
