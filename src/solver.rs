use crate::color::Color;
use crate::dodeca::Dodeca;
use std::collections::VecDeque;
use std::io::{self, Read, Write};

pub struct Solver {
    mem: Box<[Option<Color>]>,
    queue: VecDeque<usize>,
    filled: usize,
}

impl Solver {
    pub const ROOT: usize = Dodeca::SOLVED_INDEX;
    pub const MAX_DIST: usize = 34; // see bin/superflip.rs

    pub fn new() -> Self {
        let mut mem = vec![None; Dodeca::TWELVE_FAC].into_boxed_slice();
        mem[Self::ROOT] = Some(Color::EMPTY);

        let mut queue = VecDeque::with_capacity(Dodeca::TWELVE_FAC / 4);
        queue.push_back(Self::ROOT);

        Self {
            mem,
            queue,
            filled: 1,
        }
    }

    pub fn from_bytes(bytes: &[u8]) -> Self {
        let mut filled = 0;
        let mem: Box<[Option<Color>]> = bytes
            .iter()
            .flat_map(|&byte| {
                let a = byte >> 4;
                let b = byte & 0xF;
                [a, b].into_iter()
            })
            .map(|i| {
                if i == 0xF {
                    None
                } else {
                    filled += 1;
                    Some(Color::try_from(i as usize).unwrap())
                }
            })
            .collect();

        if mem.len() != Dodeca::TWELVE_FAC {
            panic!(
                "Buf has wrong length, expected {}, got {}",
                Dodeca::TWELVE_FAC,
                mem.len()
            );
        }

        let mut queue = VecDeque::with_capacity(Dodeca::TWELVE_FAC / 4);
        queue.push_back(Self::ROOT);

        Self { mem, queue, filled }
    }

    pub fn import(mut r: impl Read) -> io::Result<Self> {
        let mut buf = Vec::with_capacity(Dodeca::TWELVE_FAC);
        r.read_to_end(&mut buf)?;
        Ok(Self::from_bytes(&buf))
    }

    pub fn iter_bytes(&self) -> impl Iterator<Item = u8> + '_ {
        self.mem.chunks(2).map(|chunk| {
            let a = chunk[0].map(|x| usize::from(x) as u8).unwrap_or(0xF);
            let b = chunk[1].map(|x| usize::from(x) as u8).unwrap_or(0xF);
            a << 4 | b
        })
    }

    pub fn export(&self, mut w: impl Write) -> io::Result<()> {
        let bytes: Vec<_> = self.iter_bytes().collect();
        w.write_all(&bytes)?;
        Ok(())
    }

    pub fn insert(&mut self, index: usize, color: Color) -> bool {
        if self.mem[index].is_none() {
            self.mem[index] = Some(color);
            self.filled += 1;
            true
        } else {
            false
        }
    }

    pub fn search_all(&mut self) {
        for i in 0..Dodeca::TWELVE_FAC {
            self.search_for(i);
        }
    }

    pub fn search_for(&mut self, target: usize) {
        let mut log_counter = 0;
        while self.mem[target].is_none() {
            let current_index = match self.queue.pop_front() {
                Some(i) => i,
                None => break,
            };
            let current = Dodeca::from(current_index);

            for &swap in current.possible_swaps() {
                let swapped = current.after_swap(swap);
                let swapped_index = usize::from(swapped);

                if self.insert(swapped_index, current.empty()) {
                    self.queue.push_back(swapped_index);
                }

                Self::log(
                    log_counter,
                    true,
                    &[("searched", self.filled), ("queued", self.queue.len())],
                );
                log_counter += 1;
            }
        }
    }

    pub fn path(&self, start: usize) -> Option<Vec<Color>> {
        let mut swaps = Vec::with_capacity(Self::MAX_DIST);
        let mut current = Dodeca::from(start);
        let mut current_index = start;
        while current_index != Self::ROOT {
            let swap = self.mem[current_index]?;
            swaps.push(swap);

            current.swap(swap);
            current_index = usize::from(current);
        }
        Some(swaps)
    }

    pub fn successor(&self, index: usize) -> Option<usize> {
        if index == Self::ROOT {
            Some(Self::ROOT)
        } else {
            let mut dodeca = Dodeca::from(index);
            let swap = self.mem[index]?;
            dodeca.swap(swap);
            Some(dodeca.into())
        }
    }

    pub fn compute_dists(&self) -> Box<[usize]> {
        let mut stack = Vec::new();
        let mut dists = vec![usize::MAX; Dodeca::TWELVE_FAC].into_boxed_slice();
        dists[Self::ROOT] = 0;

        for i in 0..Dodeca::TWELVE_FAC {
            Self::log(i, false, &[("searched", i)]);

            let mut top = i;
            stack.push(top);
            while dists[top] == usize::MAX {
                top = self.successor(top).unwrap();
                stack.push(top);
            }

            let mut dist = dists[top];
            while let Some(x) = stack.pop() {
                dists[x] = dist;
                dist += 1;
            }
        }
        eprintln!("");
        dists
    }

    fn format_human(data: &[(&'static str, usize)]) -> String {
        const FACTOR: f32 = 100.0 / Dodeca::TWELVE_FAC as f32;
        data.iter()
            .copied()
            .map(|(s, x)| format!("{}: {:4.1}%", s, FACTOR * x as f32))
            .collect::<Vec<_>>()
            .join(" - ")
    }

    fn format_csv(data: &[(&'static str, usize)]) -> String {
        const FACTOR: f32 = 1.0 / Dodeca::TWELVE_FAC as f32;
        data.iter()
            .copied()
            .map(|(_, x)| format!("{:.8}", FACTOR * x as f32))
            .collect::<Vec<_>>()
            .join(", ")
    }

    fn log(i: usize, csv: bool, data: &[(&'static str, usize)]) {
        if i % (Dodeca::TWELVE_FAC / 1000) == 0 {
            eprint!("\r{}", Self::format_human(data));
            io::stderr().flush().unwrap();

            if csv {
                println!("{}", Self::format_csv(data));
            }
        }
    }
}
