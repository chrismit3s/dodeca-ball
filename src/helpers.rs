use std::time::{Duration, Instant};

pub fn try_stopwatch<F, T, E>(f: F) -> Result<(T, Duration), E>
where
    F: FnOnce() -> Result<T, E>,
{
    let start = Instant::now();
    let ret = f();
    let elapsed = start.elapsed();
    ret.map(|t| (t, elapsed))
}

pub fn stopwatch<F, T>(f: F) -> (T, Duration)
where
    F: FnOnce() -> T,
{
    let start = Instant::now();
    let ret = f();
    let elapsed = start.elapsed();
    (ret, elapsed)
}

pub fn format_duration(d: Duration, n: usize) -> String {
    const UNITS: &'static [(&'static str, u128)] = &[
        ("ns", 1000),
        ("us", 1000),
        ("ms", 1000),
        ("s", 60),
        ("min", 60),
        ("h", u128::MAX),
    ];

    UNITS
        .iter()
        .scan(d.as_nanos(), |remaining, (unit, size)| {
            let value = *remaining % size;
            *remaining /= size;
            Some((value, unit))
        })
        .filter(|&(v, _)| v > 0)
        .collect::<Vec<_>>()
        .into_iter()
        .rev()
        .take(n)
        .map(|(v, u)| format!("{}{}", v, u))
        .reduce(|s, vu| s + " " + &vu)
        .unwrap_or_else(|| String::from("0ns"))
}
