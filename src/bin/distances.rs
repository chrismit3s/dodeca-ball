use dodecaball::dodeca::Dodeca;
use dodecaball::helpers;
use dodecaball::solver::Solver;
use std::env;
use std::fs::File;
use std::io;

fn main() -> io::Result<()> {
    let args: Vec<_> = env::args().collect();
    if args.len() > 2 {
        eprintln!("USAGE: {} [<solver-file>]", args[0]);
    }

    let (solver, elapsed) = helpers::try_stopwatch(|| match args.get(1) {
        Some(path) => Solver::import(File::open(path)?),
        None => Ok(Solver::new()),
    })?;

    eprintln!(
        "creating solver took {}",
        helpers::format_duration(elapsed, 2)
    );

    let (dists, elapsed) = helpers::stopwatch(|| solver.compute_dists());

    eprintln!(
        "counting all distances took {}",
        helpers::format_duration(elapsed, 2)
    );

    let mut counts = [0; Solver::MAX_DIST + 1];
    dists.iter().copied().for_each(|dist| counts[dist] += 1);

    for (dist, abs) in counts.into_iter().enumerate() {
        let rel = abs as f32 / Dodeca::TWELVE_FAC as f32;
        eprintln!("Distance {:2}: {:4.1}% ({})", dist, 100.0 * rel, abs);
        println!("{}, {}", dist, rel);
    }

    Ok(())
}
