use dodecaball::color::Color;
use dodecaball::dodeca::Dodeca;
use dodecaball::helpers;
use dodecaball::solver::Solver;
use std::env;
use std::fs::File;
use std::io::{self, Write};

fn main() -> io::Result<()> {
    let args: Vec<_> = env::args().collect();
    if args.len() > 2 {
        eprintln!("USAGE: {} [<solver-file>]", args[0]);
    }

    let (mut solver, elapsed) = helpers::try_stopwatch(|| match args.get(1) {
        Some(path) => Solver::import(File::open(path)?),
        None => Ok(Solver::new()),
    })?;

    eprintln!(
        "creating solver took {}",
        helpers::format_duration(elapsed, 2)
    );

    let dodeca = Dodeca::new(read_layout());

    let (solution, elapsed) = helpers::stopwatch(|| dodeca.solve_with(&mut solver));
    eprintln!("found solution in {}", helpers::format_duration(elapsed, 2));

    match solution {
        None => eprintln!("Impossible to solve"),
        Some(swaps) => {
            eprintln!("Solution: {} moves", swaps.len());
            for (i, swap) in swaps.iter().enumerate() {
                eprintln!("{:>2}. {}", i + 1, swap);
            }
        }
    }

    Ok(())
}

fn add_to_layout(layout: &mut [Color; Color::NUM_COLORS], prompt: &str, color: Color) {
    layout[usize::from(color)] = read_color(prompt, color);
}

fn read_layout() -> [Color; Color::NUM_COLORS] {
    let mut layout = [Color::EMPTY; Color::NUM_COLORS];
    let empty = read_color("empty? ", Color::EMPTY);
    let on_top = empty.opposite();

    for &neighbor in empty.neighbors() {
        add_to_layout(&mut layout, &format!("{}? ", neighbor), neighbor);
    }

    add_to_layout(&mut layout, &format!("{}? ", on_top), on_top);

    for &neighbor in on_top.neighbors() {
        add_to_layout(&mut layout, &format!("{}? ", neighbor), neighbor);
    }
    layout
}

fn read_color(prompt: &str, default: Color) -> Color {
    let mut buffer = String::new();
    loop {
        buffer.clear();
        eprint!("{}", prompt);
        io::stdout().flush().unwrap();
        io::stdin().read_line(&mut buffer).unwrap();

        let trimmed = buffer.trim();
        let parsed: Result<Color, _> = trimmed.parse();
        match parsed {
            Err(_) if trimmed.len() == 0 => break default,
            Err(err) => eprintln!("Error: {}", err),
            Ok(color) => break color,
        }
    }
}
