use dodecaball::helpers;
use dodecaball::solver::Solver;
use std::env;
use std::error::Error;
use std::fs::File;

fn main() -> Result<(), Box<dyn Error>> {
    let args: Vec<_> = env::args().collect();
    if args.len() != 2 {
        let msg = format!(
            "USAGE: {} <solver-file>",
            args.get(0).map(String::as_str).unwrap_or("create.exe")
        );
        eprintln!("{}", msg);
        return Err(msg.into());
    }

    let file = File::create(&args[1])?;
    let mut solver = Solver::new();

    let (_, elapsed) = helpers::stopwatch(|| solver.search_all());
    eprintln!("Search took {}", helpers::format_duration(elapsed, 2));

    let (_, elapsed) = helpers::try_stopwatch(|| solver.export(file))?;
    eprintln!("Export took {}", helpers::format_duration(elapsed, 2));

    Ok(())
}
