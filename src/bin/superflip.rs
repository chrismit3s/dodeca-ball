use dodecaball::dodeca::Dodeca;
use dodecaball::helpers;
use dodecaball::solver::Solver;
use std::env;
use std::fs::File;
use std::io;

fn main() -> io::Result<()> {
    let args: Vec<_> = env::args().collect();
    if args.len() > 2 {
        eprintln!("USAGE: {} [<solver-file>]", args[0]);
    }

    let (solver, elapsed) = helpers::try_stopwatch(|| match args.get(1) {
        Some(path) => Solver::import(File::open(path)?),
        None => Ok(Solver::new()),
    })?;

    eprintln!(
        "creating solver took {}",
        helpers::format_duration(elapsed, 2)
    );

    let (dists, elapsed) = helpers::stopwatch(|| solver.compute_dists());

    eprintln!(
        "searching for superflip took {}",
        helpers::format_duration(elapsed, 2)
    );

    let max = dists.iter().copied().max().unwrap();
    let swaps = solver.path(max).unwrap();
    eprint!("Most distant layout: {}\n{}", max, Dodeca::from(max));
    eprintln!("Solution: {} moves", swaps.len());
    for (i, swap) in swaps.iter().enumerate() {
        eprintln!("{:>2}. {}", i + 1, swap);
    }

    Ok(())
}
